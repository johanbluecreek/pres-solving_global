# Solving Global Optimisation Problems

## Abstract

When answers are difficult to come by via analytical means, they still may be possible to find by formulating it as an optimisation problem and finding numerical solutions. I will talk about evolutionary computing algorithms, differential evolution in particular, and demonstrate how it can be used to answer questions related to the landscape/swampland. I will recommend software and mention design strategies so that anyone can start applying these methods themselves.

## Information

In this repository you will find not only the presentation but under `notebook/` you will find a Julia Jupyter notebook that was used to generate the videos for the presentation.

This presentation is best viewed with `pdfpc` (https://github.com/jakobwesthoff/Pdf-Presenter-Console) to see animations. The animations are otherwise separate files you can find above. You can also find a version of the talk without animations and images instead in the branch `still` above (drop-down menu that says "master").

This talk was given at:

 * The "de Sitter Constructions in String Theory" workshop at IPhT CEA/Saclay, France, https://indico.in2p3.fr/event/19722/, @ 2019-12-11 (tag: saclay-20191211)
